<h1 align="center">COVID-19-APP</h1>
<p align="center">Este projeto <strong>COVID-19-APP</strong> é desenvolvido com finalidade de testar a API de informações do coronavírus e realizar um estudo em React, NodeJS e NextJS. A API que está sendo usada é a <a href="https://github.com/mathdroid/covid-19-api">COVID-19-API</a>.</p>
<p align="center">
  <a aria-label="Node" href="https://github.com/nodejs/node/blob/master/doc/changelogs/CHANGELOG_V12.md#12.18.2">
    <img src="https://img.shields.io/badge/node.js@lts-12.18.2-informational?logo=Node.JS"></img>
  </a>
  <a aria-label="Next" href="https://nextjs.org/blog/next-9-3">
    <img src="https://img.shields.io/badge/nextjs-9.3-informational"></img>
  </a>
</p>

# Instalação e execução

_ps: Se precisar de ajuda para fazer um clone, esse [tutorial aqui](https://help.github.com/pt/github/creating-cloning-and-archiving-repositories/cloning-a-repository) vai te ajudar_

1. Abra o terminal do seu computador.
1. Faça um clone desse repositório rodando: <br> `git clone https://github.com/leandrocunha/covid-19-app`;
1. Entre na pasta rodando pelo terminal: `cd covid-19-app`;
1. Rode `npm i` ou `npm install` para instalar as dependências do projeto;
1. Rode `npm start` para iniciar o servidor de desenvolvimento.

# Como contribuir

Se quiser contribuir para esse repositório aqui, seja corrigindo algum problema, adicionando comentários ou melhorando a documentação, você pode seguir esse tutorial abaixo:

- Faça [um fork](https://help.github.com/pt/github/getting-started-with-github/fork-a-repo) desse repositório;
- Entre no seu perfil no GitHub e faça um clone do repositório que você fez um *fork*;
- Crie uma *branch* com a sua alteração: `git checkout -b minha-alteracao`;
- Faça as alterações necessárias no código ou na documentação;
- Faça *commit* das suas alterações: `git commit -m 'feat: Minha nova feature'`;
- Faça *push* para a sua *branch*: `git push origin minha-alteracao`;
- Agora é só abrir a sua *pull request* no repositório que você fez o *fork*;

Depois que o *merge* da sua *pull request* for feito, você pode deletar a sua *branch*.

O projeto é um fork com algumas alterações e melhorias do Projeto de Mayk Brito [covid-19](https://github.com/maykbrito/covid-19).

# Licença

Esse projeto está sob a licença [MIT](LICENSE.md). 
