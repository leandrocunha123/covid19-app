import Image from 'next/image'
import styled from 'styled-components'

import logo from '../images/virus.png'

const Logo = styled.header`
    margin: 32px auto 0;
    width: 200px;
    
    display: flex;
    place-items: center;
    
    font-size: 24px;
    
    img {
       width: 60px;
       margin-right: 8px;
    }
`

export default function Header(){
    return <Logo>
        <Image src={logo} alt="Covid-19" width={60} height={60}/>
        <h3>COVID-19</h3>
        </Logo>
}
